package noobbot;

import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import java.io.File;
import java.io.FileWriter;

class Global {

    public static boolean isDebug = false;
    public static double factorK_init = 0.60;//0.5; //0.48
    public static double factorK;
    public static double maxStraightSpeed = 25.0;
    public static double INCREASE_K_FOR_RACE = 1.03;
    public static double TURBO_TRIGGER_THROTTLE = 3.3;
    public static double lookAhedForBrakingFactor = 0.9;//0.83;
    public static double turboBreakFactor = 2.0; //5.75;  5.1:lla crashasi keimolassa
    public static double increaseBrakingDistanceAfterCrash = 1.25; //Kuinka paljon aikaistetaan jarrutusta ennen mutkaa

    public static int LEFT = -1;
    public static int RIGHT = 1;
    public static int CENTER = 0;

    public static double ownCarMaxAngle = 0;

    public static double breakingSpeedMin = 999;
    public static double breakingSpeedDecreaseMin;
    public static double breakingSpeedMax = 0;
    public static double breakingSpeedDecreaseMax;
    public static double speedDecreaseK = -1;
    public static double distanceToNextSpeedDecrease; //Rumaa...

    public static void initSpeedDecreaseK() {
        speedDecreaseK = (breakingSpeedDecreaseMax - breakingSpeedDecreaseMin) / (breakingSpeedMax - breakingSpeedMin);
    }

    public static double getSpeedDecreaseForSpeed(double currentSpeed) {
        if (!breakingSystemIsInUse()) {
            return 0.12; //Arvaus
        }

        return (currentSpeed - breakingSpeedMin) * speedDecreaseK + breakingSpeedDecreaseMin;
    }

    static boolean breakingSystemIsInUse() {
        return !((speedDecreaseK == -1) || (breakingSpeedMin == 999) || (breakingSpeedMax == 0));
    }
}

//JSON classes
class JSON_CarID {

    public String name;
    public String color;

    @Override
    public String toString() {
        return "car: " + name + " color: " + color;
    }

    public boolean isSameAs(JSON_CarID car2ID) {
        return (car2ID.name.equals(this.name) && car2ID.color.equals(this.color));
    }
}

class JSON_PositionLane {

    public int startLaneIndex;
    public int endLaneIndex;
}

class JSON_PiecePosition {

    public int pieceIndex;
    public double inPieceDistance;
    public JSON_PositionLane lane;
    public int lap;
}

class JSON_CarPositions {

    public List<JSON_CarPosition> data;
    public String gameId;
    public int gameTick;
}

class JSON_CarPosition {

    public JSON_CarID id;
    public double angle;
    public JSON_PiecePosition piecePosition;

    @Override
    public String toString() {
        return "CarPosition " + id + " lap:" + piecePosition.lap + " angle:" + angle;
    }
}

class JSON_GameInit {

    public JSON_Race race;
}

class JSON_RaceSession {

    public int durationMs; //aika-ajo
    public int laps; //kisa
    public int maxLapTimeMs; //kisa
    public boolean quickRace; //kisa
}

class JSON_CarDimensions {

    public double length;
    public double width;
    public double guideFlagPosition;

    @Override
    public String toString() {
        return "length=" + length + " width=" + width;
    }
}

class JSON_TurboAvailable {

    public double turboDurationMilliseconds;
    public int turboDurationTicks;
    public double turboFactor;
}

class JSON_Car {

    public JSON_CarID id;
    public JSON_CarDimensions dimensions;
}

class JSON_Race {

    public JSON_Track track;
    public List<JSON_Car> cars;
    public JSON_RaceSession raceSession;
}

class JSON_Track {

    public String id;
    public String name;
    public List<JSON_TrackPiece> pieces;
    public List<JSON_Lane> lanes;

    @Override
    public String toString() {
        return "JSON_Track=" + name;
    }
}

class JSON_TrackPiece {

    public double length;
    @SerializedName("switch")
    public boolean isSwitch;
    public double radius;
    public double angle;
}

class JSON_Lane {

    public int index;
    public double distanceFromCenter;

    @Override
    public String toString() {
        return "Lane index=" + this.index + "  Distance=" + this.distanceFromCenter;
    }
}

/* omaan kayttoon */
class SpeedRecorder {

    private double maxSpeed = 0.0;
    private double speedSum = 0.0;
    private int speedCount = 0;

    public SpeedRecorder() {

    }

    public void addSpeed(double speed) {
        speedCount++;
        speedSum += speed;
        if (speed > maxSpeed) {
            maxSpeed = speed;
        }
    }

    public double maxSpeed() {
        return maxSpeed;
    }

    public double averageSpeed() {
        if (speedCount == 0) {
            return 0;
        }
        return speedSum / speedCount;
    }

    @Override
    public String toString() {
        return "Max speed=" + this.maxSpeed + "  Average=" + this.averageSpeed();
    }
}

class AngleRecorder {

    private double maxAngle = 0.0;
    private double minAngle = 999.0;
    private double angleSum = 0.0;
    private int angleCount = 0;

    public AngleRecorder() {

    }

    public void addAngle(double angle) {
        angleCount++;
        angleSum += angle;
        if (angle > maxAngle) {
            maxAngle = angle;
        }
        if (angle < minAngle) {
            minAngle = angle;
        }
    }

    public double maxAngle() {
        return maxAngle;
    }

    public double minAngle() {
        return minAngle;
    }

    public double averageAngle() {
        if (angleCount == 0) {
            return 0;
        }
        return angleSum / angleCount;
    }

    @Override
    public String toString() {
        return "Max angle=" + this.maxAngle + "  Average=" + this.averageAngle() + " min=" + this.minAngle;
    }
}

class AllCars {

    private ArrayList<Car> cars = new ArrayList<>();

    public ArrayList<Car> getCars() {
        return this.cars;
    }

    /*
     public Car findCarWithLog(JSON_CarID carID) {
     int index = 0;
     for (Car car : this.cars) {
     if (car.isSameAs(carID) ) {
     System.out.println("FOUND AllCars.findCar "+carID+"  foundOnIndex:"+index+"   "+car);
     return car;
     }
     else
     System.out.println("NOT FOUND AllCars.findCar "+carID+"  foundOnIndex:"+index+"   "+car); 
     index++;
     }
     return null;
     }*/
    public Car findCar(JSON_CarID carID) {
        for (Car car : this.cars) {
            if (car.isSameAs(carID)) {
                return car;
            }
        }
        return null;
    }

    public void initRace() {
        for (Car car : this.cars) {
            car.initRace();
        }
    }

    public void removeAllCars() {
        while (cars.size() > 0) {
            cars.remove(0);
        }
    }

    public void removeCar(Car car) {
        cars.remove(car);
    }

    public Car myCar() {
        for (Car car : this.cars) {
            if (car.getIsMyCar()) {
                return car;
            }
        }
        return null;
    }

    public Car addCar(JSON_CarID carID, JSON_CarDimensions dimensions) {
        Car addedCar = new Car(carID);
        addedCar.setDimensions(dimensions);
        System.out.println("Added car " + carID + " dim " + dimensions);
        this.cars.add(addedCar);
        return addedCar;
    }

    public Car addCar(Car car) {
        this.cars.add(car);
        return car;
    }

    public void reportStatistics() {
        for (Car car : this.cars) {
            System.out.println(car + "  " + car.getSpeedRecorder() + "  crashes:" + car.getCrashCount()); //TODO laitetaan ehto, voidaanko tulosta
        }
    }
}

class Car {

    private JSON_CarID id;
    private JSON_CarDimensions dimensions;
    private double angle;
    private int currentLap;
    private boolean isMyCar = false;
    private double travelledDistancePreviousPieces = 0;
    private double travelledDistanceCurrent = 0;
    private double lastTravelledDistance = 0;
    private double distanceOnThisLap = 0; //Tama on maaliviivalta matkattu matka, eli kaytetaan ohistutarkistuksissa
    private double travelledInThisPiece = 0;
    private double travelledDistancePreviousPiecesOnThisLap = 0;
    private TrackPiece lastPiece = null; //Missa palassa on nahty viimeksi ajavan
    private int lastStartLaneIndex;
    private int lastEndLaneIndex;
    private int lastGameTick = 0;
    private int lastLap = -1;
    private double speed = 0;
    private SpeedRecorder speedRecorder = new SpeedRecorder();
    private TrackPieceLane currentTrackPieceLane;  //Paivitetaan updateTravelledDistancessa
    private int currentLaneIdx; //Huomioi myos kesken palaa tehdyn kaistavaihdon, paivitetaan updateTravelledDistancessa
    private int tobeLaneIdx;
    private int crashCount = 0;

    public Car(JSON_CarID id) {
        this.id = id;
    }

    public void initRace() {
        this.travelledDistancePreviousPieces = 0;
        this.travelledDistancePreviousPiecesOnThisLap = 0;
        this.distanceOnThisLap = 0;
    }

    //Setterit ja Getterit alkaa
    public void setCrashCount(int crashCount) {
        this.crashCount = crashCount;
    }

    public int getCrashCount() {
        return crashCount;
    }

    public JSON_CarID getId() {
        return id;
    }

    public void setId(JSON_CarID id) {
        this.id = id;
    }

    public JSON_CarDimensions getDimensions() {
        return dimensions;
    }

    public void setDimensions(JSON_CarDimensions dimensions) {
        this.dimensions = dimensions;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public boolean getIsMyCar() {
        return this.isMyCar;
    }

    public void setIsMyCar(boolean isMyCar) {
        this.isMyCar = isMyCar;
    }

    public double getTravelledDistancePreviousPieces() {
        return travelledDistancePreviousPieces;
    }

    public void setTravelledDistancePreviousPieces(double travelledDistancePreviousPieces) {
        this.travelledDistancePreviousPieces = travelledDistancePreviousPieces;
    }

    public double getTravelledDistanceCurrent() {
        return travelledDistanceCurrent;
    }

    public void setTravelledDistanceCurrent(double travelledDistanceCurrent) {
        this.travelledDistanceCurrent = travelledDistanceCurrent;
    }

    public double getTravelledInThisPiece() {
        return travelledInThisPiece;
    }

    public void setTravelledInThisPiece(double travelledInThisPiece) {
        this.travelledInThisPiece = travelledInThisPiece;
    }

    public TrackPiece getLastPiece() {
        return lastPiece;
    }

    public void setLastPiece(TrackPiece lastPiece) {
        this.lastPiece = lastPiece;
    }

    public int getLastStartLaneIndex() {
        return lastStartLaneIndex;
    }

    public void setLastStartLaneIndex(int lastStartLaneIndex) {
        this.lastStartLaneIndex = lastStartLaneIndex;
    }

    public int getLastEndLaneIndex() {
        return lastEndLaneIndex;
    }

    public void setLastEndLaneIndex(int lastEndLaneIndex) {
        this.lastEndLaneIndex = lastEndLaneIndex;
    }

    public int getLastGameTick() {
        return lastGameTick;
    }

    public void setLastGameTick(int lastGameTick) {
        this.lastGameTick = lastGameTick;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public SpeedRecorder getSpeedRecorder() {
        return speedRecorder;
    }

    public void setSpeedRecorder(SpeedRecorder speedRecorder) {
        this.speedRecorder = speedRecorder;
    }

    public TrackPieceLane getCurrentTrackPieceLane() {
        return currentTrackPieceLane;
    }

    public void setCurrentTrackPieceLane(TrackPieceLane currentTrackPieceLane) {
        this.currentTrackPieceLane = currentTrackPieceLane;
    }

    public int getCurrentLaneIdx() {
        return currentLaneIdx;
    }

    public void setCurrentLaneIdx(int currentLaneIdx) {
        this.currentLaneIdx = currentLaneIdx;
    }

    public int getTobeLaneIdx() {
        return tobeLaneIdx;
    }

    public void setTobeLaneIdx(int tobeLaneIdx) {
        this.tobeLaneIdx = tobeLaneIdx;
    }

    public double getLastTravelledDistance() {
        return lastTravelledDistance;
    }

    public void setLastTravelledDistance(double lastTravelledDistance) {
        this.lastTravelledDistance = lastTravelledDistance;
    }

    public double getDistanceOnThisLap() {
        return distanceOnThisLap;
    }

    public int getCurrentLap() {
        return currentLap;
    }

    public double getTravelledDistancePreviousPiecesOnThisLap() {
        return travelledDistancePreviousPiecesOnThisLap;
    }
    //Setterit ja Getterit paattyy

    public double speedLimitAfterDistance(double travelDistance) { //TODO korjaa kayttamaan oikeasti etaisyytta
        // return this.currentTrackPieceLane.getNext().getMaxSpeed(); Nolo fixi

        double distanceLeftOnThisPiece = this.currentTrackPieceLane.getPieceLength() - this.travelledInThisPiece;
        double minSpeedLimit = this.currentTrackPieceLane.getMaxSpeed();

        if (travelDistance <= distanceLeftOnThisPiece) {
            return minSpeedLimit;
        }
        travelDistance -= distanceLeftOnThisPiece;

        TrackPieceLane nextTrackPieceLane = currentTrackPieceLane.getNext();

        while (travelDistance > 0) {
            if (nextTrackPieceLane.getMaxSpeed() < minSpeedLimit) {
                minSpeedLimit = nextTrackPieceLane.getMaxSpeed();
            }
            if (travelDistance < nextTrackPieceLane.getPieceLength()) {
                return minSpeedLimit;
            }
            travelDistance -= nextTrackPieceLane.getPieceLength();
            nextTrackPieceLane = nextTrackPieceLane.getNext();
        }
        return minSpeedLimit;

    }

    public double nextSpeedDecrease() {
        double distanceToNextSpeedDecrease = this.currentTrackPieceLane.getPieceLength() - this.travelledInThisPiece; //distanceLeftOnThisPiece
        double currentSpeedLimit = this.currentTrackPieceLane.getMaxSpeed();
        TrackPieceLane nextTrackPieceLane = currentTrackPieceLane.getNext();
        int cnt = 0;
        while (true) {
            cnt++;
            if (cnt > 20) { //Estetään jumi pelin lopussa
                Global.distanceToNextSpeedDecrease = distanceToNextSpeedDecrease;
                return currentSpeedLimit;
            }
            if (nextTrackPieceLane.getMaxSpeed() < currentSpeedLimit) {
                Global.distanceToNextSpeedDecrease = distanceToNextSpeedDecrease;
                return nextTrackPieceLane.getMaxSpeed();
            }
            distanceToNextSpeedDecrease += nextTrackPieceLane.getPieceLength();
            nextTrackPieceLane = nextTrackPieceLane.getNext();
        }
    }

    public boolean isSameAs(JSON_CarID car2ID) {
        return (car2ID.name.equals(this.id.name) && car2ID.color.equals(this.id.color));
    }

    public double updateTravelledDistance(Track track, JSON_CarPosition carPosition, TrackPiece currentTrackPiece, int currentGameTick) {
        this.currentLap = carPosition.piecePosition.lap;
        this.angle = carPosition.angle;
        if (this.lastPiece != null && (this.lastPiece != currentTrackPiece)) { //Lasketaan edellisen palan etaisyys mukaan travelledDistancePreviousPieces:iin
            double dist = 0;
            if (this.lastPiece.isStraight()) {
                dist = this.lastPiece.length;
            } else {
                double l1dist = this.lastPiece.getCornerLenght(this.lastStartLaneIndex);
                double l2dist = this.lastPiece.getCornerLenght(this.lastEndLaneIndex);
                dist = ((l1dist + l2dist) / 2.0);
            }

            if (dist == 0) {
                System.out.println("** PROBLEM ... dist = 0");
            }
            this.travelledDistancePreviousPieces += dist;
            if (carPosition.piecePosition.lap != lastLap) {
                lastLap = carPosition.piecePosition.lap;
                this.travelledDistancePreviousPiecesOnThisLap = 0; //Nollataan joka kierroksella
            }
            this.travelledDistancePreviousPiecesOnThisLap += dist;
            //if ( this.travelledDistancePreviousPiecesOnThisLap < 200 )
            // System.out.println("Adding "+dist+" to "+this.travelledDistancePreviousPiecesOnThisLap);
        }

        if (lastLap == -1) {
            lastLap = carPosition.piecePosition.lap;
        }

        this.travelledInThisPiece = carPosition.piecePosition.inPieceDistance;
        this.travelledDistanceCurrent = this.travelledDistancePreviousPieces + this.travelledInThisPiece;
        this.distanceOnThisLap = this.travelledDistancePreviousPiecesOnThisLap + this.travelledInThisPiece;
        this.lastPiece = currentTrackPiece;

        //Lasketaan nopeus
        double diffDistance = this.travelledDistanceCurrent - this.lastTravelledDistance;
        double diffTime = (double) currentGameTick - (double) this.lastGameTick;
        if (diffTime == 0) {
            this.speed = 0;
        } else {
            if (diffTime > 0) {
                this.speed = diffDistance / diffTime;
            } else {
                this.speed = diffDistance / 1.0;
            }
        }

        if (this.speed < 0) {
            System.out.println("SPEED PROBLEM!! diffDistance=" + diffDistance + "   diffTime=" + diffTime + " UPDATING FOR CAR " + this.id + " " + carPosition + " " + currentTrackPiece);
            System.out.println("travelledDistancePreviousPieces=" + this.travelledDistancePreviousPieces + " travelledInThisPiece=" + this.travelledInThisPiece);
        }

        this.speedRecorder.addSpeed(this.speed);

        this.lastStartLaneIndex = carPosition.piecePosition.lane.startLaneIndex;
        this.lastEndLaneIndex = carPosition.piecePosition.lane.endLaneIndex;
        this.lastTravelledDistance = this.travelledDistanceCurrent;
        this.lastGameTick = currentGameTick;

        //Lisataan nopeus myos trackpiecen lanelle
        this.currentLaneIdx = this.lastStartLaneIndex; //ollaan palan alussa
        if (carPosition.piecePosition.inPieceDistance > (currentTrackPiece.getPieceLength(this.lastEndLaneIndex) / 2)) {
            this.currentLaneIdx = this.lastEndLaneIndex;
        }
        this.tobeLaneIdx = this.lastEndLaneIndex;
        this.currentTrackPieceLane = currentTrackPiece.getTrackPieceLane(this.currentLaneIdx);
        double pieceLengthOnThisTrack = this.currentTrackPieceLane.getPieceLength();
        double thirdOfPieceLength = pieceLengthOnThisTrack / 3;
        //Ollaan keskikolmanneksessa, eli tallennetaan kulma -- Mutta vain omalle
        if (this.isMyCar) {
            if (carPosition.angle > Global.ownCarMaxAngle) {
                Global.ownCarMaxAngle = carPosition.angle;
            }
            if ((this.travelledInThisPiece >= thirdOfPieceLength) && (this.travelledInThisPiece <= thirdOfPieceLength * 2)) {
                this.currentTrackPieceLane.recordAngle(carPosition.angle);
            }
        }

        this.currentTrackPieceLane.updateSpeedForCarIndex(this.currentLaneIdx, this.speed);

        return this.travelledDistanceCurrent;
    }

    @Override
    public String toString() {
        return "Car=" + this.id;
    }
}

class CarDimensions {

    private double length;
    private double width;
    private double guideFlagPosition;

    public CarDimensions(double length, double width, double guideFlagPosition) {
        this.length = length;
        this.width = width;
        this.guideFlagPosition = guideFlagPosition;
    }

    //Setterit ja Getterit alkaa
    public void setLength(double length) {
        this.length = length;
    }

    public double getLength() {
        return length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getGuideFlagPosition() {
        return guideFlagPosition;
    }

    public void setGuideFlagPosition(double guideFlagPosition) {
        this.guideFlagPosition = guideFlagPosition;
    }

    public double getWidth() {
        return width;
    }

    //Setterit ja Getterit paattyy
}

class Track {

    //public String id;
    //public String name;
    private AllCars allCars;
    private Car myCar;
    private AllCars opponentCars;
    private List<TrackPiece> pieces = new ArrayList<>();
    private List<Lane> lanes = new ArrayList<>();
    private int trackPieceCount; //Montako palaa radassa on yhdella kierroksella

    public Track(AllCars allCars) {
        this.allCars = allCars;
        this.opponentCars = new AllCars();
        this.myCar = null;
    }

    @Override
    public String toString() {
        return "Autoja radalla: " + this.allCars.getCars().size() + " kaistoja: " + this.lanes.size() + " paloja/kierros: " + this.trackPieceCount;
    }

    public void fillOpponents() {
        System.out.println("fillOpponents");
        this.opponentCars.removeAllCars();
        for (Car oneCar : this.allCars.getCars()) {
            if (!oneCar.getIsMyCar()) {
                this.opponentCars.addCar(oneCar);
            }
        }
    }

    //Setterit ja Getterit alkaa
    public AllCars getAllCars() {
        return this.allCars;
    }

    public void setMyCar(Car car) {
        this.myCar = car;
        System.out.println("MY CAR = " + this.myCar);
    }

    public AllCars getOpponentCars() {
        return this.opponentCars;
    }

    public void setAllCars(AllCars allCars) {
        this.allCars = allCars;
    }

    public List<TrackPiece> getPieces() {
        return pieces;
    }

    public void setPieces(List<TrackPiece> pieces) {
        this.pieces = pieces;
    }

    public List<Lane> getLanes() {
        return lanes;
    }

    public void setLanes(List<Lane> lanes) {
        this.lanes = lanes;
    }

    public int getTrackPieceCount() {
        return trackPieceCount;
    }

    public void setTrackPieceCount(int trackPieceCount) {
        this.trackPieceCount = trackPieceCount;
    }

    public Lane getLane(int laneIndex) {
        for (Lane oneLane : this.lanes) {
            if (oneLane.index == laneIndex) {
                return oneLane;
            }
        }
        assert (false);
        return null;
    }

    public TrackPiece getPiece(int pieceIndex, int lap) {
        if (lap < 0) //Alussa voi tulla negatiivisia kierroksia
        {
            lap = 0;
        }
        int idx = lap * trackPieceCount + pieceIndex;
        if (idx < 0) {
            //System.out.println("*** getPiece PROBLEM "+pieceIndex+" "+lap+"="+idx);
            idx = 0;
        }
        if (idx >= this.pieces.size()) {
            lap = lap - 1;
            idx = lap * trackPieceCount + pieceIndex;
            //System.out.println("*** getPiece PROBLEM "+pieceIndex+" "+lap+"="+idx);
        }
        return this.pieces.get(idx);
    }

    public void recalculateMaxSpeeds() {
        for (TrackPiece piece : pieces) {
            piece.recalculateMaxSpeeds();
        }
    }

    public boolean isThereOtherCarBehindCar(Car oneCar) {
        JSON_CarDimensions myDim = oneCar.getDimensions();
        double myLength = myDim.length;
        double guideFlag = myDim.guideFlagPosition;
        double backDistance = oneCar.getDistanceOnThisLap() + guideFlag - myLength; //Auton pera
        int carLineIdx = oneCar.getCurrentLaneIdx();
        for (Car secondCar : this.allCars.getCars()) {
            if (secondCar.getCurrentLaneIdx() == carLineIdx && (!secondCar.isSameAs(oneCar.getId()))) { //Ollaan samalla kaistalla
                double secondCarDistance = secondCar.getDistanceOnThisLap(); //oletetaan, etta kaikki autot on yhta pitkia
                if ((secondCarDistance >= backDistance - myLength) && (secondCarDistance < backDistance)) {
                    return true;
                }
            }
        }
        return false;
    }

    //Setterit ja Getterit paattyy
    public int slowCarsAhead() { //Palauttaa numeron, montako autoa blokkaa kaistaamme
        int cnt = 0;
        JSON_CarDimensions myDim = this.myCar.getDimensions();
        double myLength = myDim.length;
        double guideFlag = myDim.guideFlagPosition;
        double frontDistance = this.myCar.getDistanceOnThisLap() + guideFlag; //TODO: distanceOnThisLap ei ihan toimi kierroksen vaihdoksen yhteydessa
        int ahead1 = 0; //suoraan edessa
        int ahead2 = 0; //1 paikka toisen edessa
        int ahead3 = 0; //2. paikka toisen edessa
        int myCarLineIdx = this.myCar.getCurrentLaneIdx();
        //System.out.println(myLength+" "+guideFlag+" "+frontDistance+" "+myCarLineIdx);

        for (Car oneCar : this.opponentCars.getCars()) {
            if (oneCar.getCurrentLaneIdx() == myCarLineIdx) {
                double oneCarDistance = oneCar.getDistanceOnThisLap(); //oletetaan, etta kaikki autot on yhta pitkia
                if ((oneCarDistance >= frontDistance) && (oneCarDistance < frontDistance + myLength * 1.2)) {
                    ahead1 = 1;
                }

                if ((oneCarDistance >= frontDistance + myLength) && (oneCarDistance < frontDistance + myLength * 2.1)) {
                    ahead2 = 1;
                }

                if ((oneCarDistance >= frontDistance + myLength * 2) && (oneCarDistance < frontDistance + myLength * 3.1)) {
                    ahead3 = 1;
                }
            }
        }
        if (ahead1 == 0) {
            return 0;
        }
        if (ahead2 == 0) // 1 edella
        {
            return 1;
        }
        return ahead1 + ahead2 + ahead3;
    }

    public int getFreeLaneIdx() { //Etsii kaistan, joka on muu kuin omamme ja jolla on tilaa edessa
        int myLaneIdx = this.myCar.getCurrentLaneIdx();
        int bestLaneIdx = 0;
        int bestLaneCars = 10;
        for (int laneIdx = 0; laneIdx < lanes.size(); laneIdx++) {
            if (laneIdx != myLaneIdx) {
                JSON_CarDimensions myDim = this.myCar.getDimensions();
                double myLength = myDim.length;
                double guideFlag = myDim.guideFlagPosition;
                double frontDistance = this.myCar.getDistanceOnThisLap() + guideFlag; //TODO: distanceOnThisLap ei ihan toimi kierroksen vaihdoksen yhteydessa
                int ahead1 = 0; //suoraan edessa
                int ahead2 = 0; //1 paikka toisen edessa
                int ahead3 = 0; //2. paikka toisen edessa

                for (Car oneCar : this.opponentCars.getCars()) {
                    if (oneCar.getCurrentLaneIdx() != myLaneIdx) { //Katsotaan vain autoja, jotka on eri kaistalla
                        double oneCarDistance = oneCar.getDistanceOnThisLap(); //oletetaan, etta kaikki autot on yhta pitkia
                        if ((oneCarDistance >= frontDistance) && (oneCarDistance < frontDistance + myLength * 1.2)) {
                            ahead1 = 1;
                        }

                        if ((oneCarDistance >= frontDistance + myLength) && (oneCarDistance < frontDistance + myLength * 2.1)) {
                            ahead2 = 1;
                        }

                        if ((oneCarDistance >= frontDistance + myLength * 2) && (oneCarDistance < frontDistance + myLength * 3.1)) {
                            ahead3 = 1;
                        }
                    }
                }
                int totalCars = ahead1 + ahead2 + ahead3;
                if (totalCars < bestLaneCars) {
                    bestLaneCars = totalCars;
                    bestLaneIdx = laneIdx;
                }
            }
        }
        return bestLaneIdx;
    }

    public int getFreeLaneBasedOnTraffic() { //Palauttaa Global.LEFT tai Global.RIGHT riippuen kumpaan suuntaan kannattaa vaihtaa
        int bestLaneIdx = this.getFreeLaneIdx();
        int currentLaneIdx = myCar.getCurrentLaneIdx();
        //Jos best = 0 ja current on 1, haluamme menna vasemmalle
        if (bestLaneIdx < currentLaneIdx) {
            return Global.LEFT;
        } else {
            return Global.RIGHT;
        }
    }

    public void removeAllPieces() {
        while (lanes.size() > 0) {
            lanes.remove(0);
        }
        while (pieces.size() > 0) {
            pieces.remove(0);
        }
    }

    public void InitRace() {
        allCars.initRace();
    }

    public TrackPiece addPiece(JSON_TrackPiece piece, int lapNumber, TrackPiece prevPiece) {
        TrackPiece newPiece = new TrackPiece(piece, lapNumber, this, prevPiece);
        this.pieces.add(newPiece);
        return newPiece;
    }

    public TrackPiece addPiece(TrackPiece trackPiece) {
        this.pieces.add(trackPiece);
        return trackPiece;
    }

    public void completeMainStarightFlagging() { //ILE korjasi
        for (int i = this.pieces.size() - 1; i >= 0; i--) {
            TrackPiece trackPiece = this.pieces.get(i);
            if (trackPiece.isStraight()) {
                trackPiece.setIsMainStraight(true);
            } else {
                break;
            }
        }
    }

    public void printPieces() {
        for (TrackPiece piece : this.pieces) {
            //System.out.println(piece);
        }
    }

    public void calculateAngleAhead() {
        int index = this.pieces.size() - 1;
        double angleAhead = 0;

        while (index >= 0) {
            if (this.pieces.get(index).angle == 0) {
                angleAhead = 0;
            }
            angleAhead += this.pieces.get(index).angle;
            this.pieces.get(index).setAngleAhead(angleAhead);
            index--;
        }
    }
}

class TrackPiece extends JSON_TrackPiece {

    private double angleAhead = 0;
    private int lapNumber;
    private boolean tightCorner;
    private boolean corner;
    private TrackPiece nextPiece;
    private TrackPiece prevPiece;
    private int cornerNumber = 0; //alkuperäisen numeroinnin mukaan
    private int pieceNumber = 0; //alkuperäisen numeroinnin mukaan
    private boolean isMainStraight = false;
    private Track track;
    private List<TrackPieceLane> trackPieceLane = new ArrayList<>();

    public TrackPiece(JSON_TrackPiece piece, int lapNumber, Track track, TrackPiece prevPiece) {
        this.angle = piece.angle;
        double ang = Math.abs(piece.angle);
        this.corner = ang > 0;
        this.tightCorner = (this.corner) && (piece.radius < 70);
        this.isSwitch = piece.isSwitch;
        this.length = piece.length;
        this.radius = piece.radius;
        this.lapNumber = lapNumber;
        this.nextPiece = this; //Kun luodaan viimeinen pala, se viittaa loputtomasti itseensa. Muille asetetaan luontivaiheessa seuraavaksi
        this.prevPiece = prevPiece;
        this.track = track;
        TrackPieceLane prevPieceLane = null;

        for (Lane oneLane : track.getLanes()) {
            TrackPieceLane addedPieceLane = new TrackPieceLane(oneLane, this, this.getPieceLength(oneLane.index));
            this.trackPieceLane.add(addedPieceLane);

            if (addedPieceLane.getOwnerTrackPiece().getPrevPiece() != null) {
                prevPiece = addedPieceLane.getOwnerTrackPiece().getPrevPiece();
            }

            if (prevPiece != null) {
                prevPieceLane = prevPiece.getTrackPieceLane(oneLane.index);
            }

            if (prevPieceLane != null) {
                prevPieceLane.setNext(addedPieceLane);
            }
            //prevPieceLane = addedPieceLane;
        }
    }

    public int getNextCornerNumber() {
        TrackPiece tp = this;
        int safeout = 0;
        boolean isCorner = tp.isCorner();
        while (safeout < 15 && (!isCorner)) {
            tp = tp.nextPiece;
            isCorner = tp.isCorner();
            safeout++;
        }
        return tp.getCornerNumber();
    }

    public int findNextCornerAndSelectLane() { //Jos tiukka mutka, mennään ulos, muuten sisään
        TrackPiece tp = this;
        int safeout = 0;
        boolean isCorner = tp.isCorner();
        while (safeout < 10 && (!isCorner)) {
            tp = tp.nextPiece;
            isCorner = tp.isCorner();
            safeout++;
        }

        if (tp.isCorner()) { //On mutka
            if (tp.isTightCorner()) { //Mennaan ulos
                if (tp.angle > 0) {
                    return Global.LEFT;
                } else {
                    return Global.RIGHT;
                }
            } else { //mennaan sisaan
                if (tp.angle < 0) {
                    return Global.LEFT;
                } else {
                    return Global.RIGHT;
                }
            }
        }
        return 0;
    }

    public int findNextTurboPlaceAndSelectLane() { //Etsii seuraavan turbopaikan ja sen jälkeisen mutkan
        TrackPiece tp = this;
        int safeout = 0;
        int canUse = tp.canUseTurbo();
        while (safeout < 30 && (canUse == 0)) {
            tp = tp.nextPiece;
            canUse = tp.canUseTurbo();
            safeout++;
        }
        return canUse;
    }

    public int canUseTurbo() {
        if (!this.isStraight()) {
            return 0;
        }

        boolean next4AreStraight = true;
        TrackPiece tp = this.nextPiece;
        for (int i = 1; i <= 4; i++) {
            if (!tp.isStraight()) {
                next4AreStraight = false;
                break;
            }
            tp = tp.nextPiece;
        }

        if (next4AreStraight == false) {
            return 0;
        }

        int safeOut = 0;
        while (tp.isStraight() && (safeOut < 30)) { //Ei etsitä liian pitkälle
            safeOut++;
            tp = tp.nextPiece;
        }

        if (tp.angle > 0) {
            return Global.LEFT;
        } else {
            return Global.RIGHT;
        }
    }

    //Setterit ja Getterit alkaa
    public boolean isCorner() {
        return corner;
    }

    public void setCornerNumber(int cornerNumber) {
        this.cornerNumber = cornerNumber;
    }

    public int getCornerNumber() {
        return cornerNumber;
    }

    public boolean isTightCorner() {
        return tightCorner;
    }

    public int getPieceNumber() {
        return pieceNumber;
    }

    public void setPieceNumber(int pieceNumber) {
        this.pieceNumber = pieceNumber;
    }

    public void setPrevPiece(TrackPiece prevPiece) {
        this.prevPiece = prevPiece;
    }

    public TrackPiece getPrevPiece() {
        return prevPiece;
    }

    public double getLength() {
        return length;
    }

    public double getRadius() {
        return radius;
    }

    public double getAngle() {
        return angle;
    }

    public double getAngleAhead() {
        return angleAhead;
    }

    public void setAngleAhead(double angleAhead) {
        this.angleAhead = angleAhead;
    }

    public int getLapNumber() {
        return lapNumber;
    }

    public void setLapNumber(int lapNumber) {
        this.lapNumber = lapNumber;
    }

    public TrackPiece getNextPiece() {
        return nextPiece;
    }

    public void setNextPiece(TrackPiece nextPiece) {
        this.nextPiece = nextPiece;
    }

    public boolean getIsMainStraight() {
        return isMainStraight;
    }

    public void setIsMainStraight(boolean isMainStraight) {
        this.isMainStraight = isMainStraight;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }

    public List<TrackPieceLane> getTrackPieceLane() {
        return trackPieceLane;
    }

    public void setTrackPieceLane(List<TrackPieceLane> trackPieceLane) {
        this.trackPieceLane = trackPieceLane;
    }

    //Setterit ja Getterit loppuu
    public double getMaxSpeedForLane(int laneInd) {
        return this.getTrackPieceLane(laneInd).getMaxSpeed();
    }

    public TrackPieceLane getTrackPieceLane(int laneIdx) {
        return trackPieceLane.get(laneIdx);
    }

    public boolean isStraight() {
        return (this.angle == 0);
    }

    public boolean isEndOfCorner() {
        return ((!this.isStraight()) && this.nextPiece.isStraight());
    }

    public void recalculateMaxSpeeds() {
        for (TrackPieceLane piecelane : trackPieceLane) {
            piecelane.calculateMaxSpeed();
        }
    }

    public double getPieceLength(int laneIndex) {
        if (this.isStraight()) {
            return this.length;
        } else {
            return this.getCornerLenght(laneIndex);
        }
    }

    public double getCornerLenght(int laneIndex) {
        Lane lane = track.getLane(laneIndex);
        //TODO, selvita onko distanceFromCenter positiivinen ja negatiivinen milla tavalla sateen suhteen/riippuvainen mutkan suunnasta (tod.nak abs() ratkaisi jo
        int cornerDirection = (int) (this.angle / Math.abs(this.angle));

        double length;
        if (cornerDirection > 0) { //Oikealle kaantyva
            length = ((Math.abs(this.angle) / 360.0) * 2 * Math.PI * (this.radius - lane.distanceFromCenter));
            //System.out.println("Kaantyy oikealle, kulma= " + this.angle+" radius=" + this.radius+" distFromCtr=" +lane.distanceFromCenter+" crnLenght=" +length);
        } else {
            length = ((Math.abs(this.angle) / 360.0) * 2 * Math.PI * (this.radius + lane.distanceFromCenter));
        }
        return length; // (a/360)*2*pi*r ja r on mutkan sade + kaistan etaisyys keskilinjasta
    }

    @Override
    public String toString() {
        return "Kulma: " + this.angle + " Kulma edessa: " + this.angleAhead + " Pituus: "
                + this.length + " Sade: " + this.radius + " On kaistanvaihto: " + this.isSwitch;
    }
}

//Radan palassa oleva kaista
class TrackPieceLane extends JSON_Lane {

    private double[] speedLimit = new double[10]; //10% valein nopeusrajoitus ko. palassa, ko. kaistalla
    private TrackPiece ownerTrackPiece;
    private List<SpeedRecorder> speedRecorderForCars = new ArrayList<>();
    private double maxSpeed;
    private double pieceLength;
    private TrackPieceLane next;
    private AngleRecorder angleRecorder = new AngleRecorder();

    public TrackPieceLane(JSON_Lane jsonLane, TrackPiece ownerTrackPiece, double pieceLength) {
        this.index = jsonLane.index;
        this.distanceFromCenter = jsonLane.distanceFromCenter;
        this.ownerTrackPiece = ownerTrackPiece;
        this.pieceLength = pieceLength;
        this.next = this; //Tama korjataan siella missa nama luodaan
        this.calculateMaxSpeed();
        for (Car oneCar : ownerTrackPiece.getTrack().getAllCars().getCars()) {
            speedRecorderForCars.add(new SpeedRecorder());
        }
    }

    //Setterit ja Getterit alkaa
    public double[] getSpeedLimit() {
        return speedLimit;
        //SpeedRecorder rec = speedRecorderForCars.get(index);
        // rec.addSpeed(speed);
        //TODO: Fixaa array out of bounds
    }

    public void recordAngle(double angle) {
        angleRecorder.addAngle(angle);
    }

    public void setSpeedLimit(double[] speedLimit) {
        this.speedLimit = speedLimit;
    }

    public TrackPiece getOwnerTrackPiece() {
        return ownerTrackPiece;
    }

    public void setOwnerTrackPiece(TrackPiece ownerTrackPiece) {
        this.ownerTrackPiece = ownerTrackPiece;
    }

    public List<SpeedRecorder> getSpeedRecorderForCars() {
        return speedRecorderForCars;
    }

    public void setSpeedRecorderForCars(List<SpeedRecorder> speedRecorderForCars) {
        this.speedRecorderForCars = speedRecorderForCars;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getPieceLength() {
        return pieceLength;
    }

    public void setPieceLength(double pieceLength) {
        this.pieceLength = pieceLength;
    }

    public TrackPieceLane getNext() {
        return this.next;
    }

    public void setNext(TrackPieceLane next) {
        this.next = next;
    }

    // public void setNext() {
    //     this.next = this.ownerTrackPiece.getNextPiece().getTrackPieceLane(this.index);
    // }
    //Setterit ja Getterit loppuu
    public void updateSpeedForCarIndex(int index, double speed) {
        //SpeedRecorder rec = speedRecorderForCars.get(index);
        // rec.addSpeed(speed);
        //TODO: Fixaa array out of bounds
    }

    public final void calculateMaxSpeed() {
        double angle = this.ownerTrackPiece.angle;
        int cornerDirection = Global.LEFT;
        if (angle > 0) {
            cornerDirection = Global.RIGHT;
        }
        double radius;

        if (cornerDirection > 0) { //Oikealle kaantyva
            radius = (this.ownerTrackPiece.radius - this.distanceFromCenter);
        } else { //Vasemmalle kaantyva
            radius = (this.ownerTrackPiece.radius + this.distanceFromCenter);
        }
        if (this.ownerTrackPiece.isStraight()) {
            this.maxSpeed = Global.maxStraightSpeed;
        } else {
            this.maxSpeed = Math.sqrt((radius * Global.factorK));
            //System.out.println("MaxSpeed=" + this.maxSpeed + " k=" + Global.factorK + " radius" + radius);
        }
        /*if ( angle<0 ) { //Mutka on vasemmalle
            
         }*/

    }
}

//Trackissa oleva lista kaistoja (tata ei ehka tarvita)
class Lane extends JSON_Lane {

    public Lane(JSON_Lane jsonLane) {
        this.index = jsonLane.index;
        this.distanceFromCenter = jsonLane.distanceFromCenter;
    }

}

public class Main {

    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
        Main main;

        if (Global.isDebug) {
            //main = new Main(reader, writer, new JoinRace(botName, botKey, "germany", 1));
            //main = new Main(reader, writer, new JoinRace(botName, botKey, "france", 1));
            //main = new Main(reader, writer, new JoinRace(botName, botKey, "usa", 1));
            //main = new Main(reader, writer, new JoinRace(botName, botKey, "japan", 1));
            main = new Main(reader, writer, new JoinRace(botName, botKey, "england", 1));
            //main = new Main(reader, writer, new Join(botName, botKey));
        } else {
            main = new Main(reader, writer, new Join(botName, botKey));
        }
    }

    final Gson gson = new Gson();
    JsonParser jsonparser = new JsonParser(); //Arrayt parsitaan talla
    private PrintWriter writer;

    public Main(final BufferedReader reader, final PrintWriter writer, final SendMsg join) throws IOException { //Vanha (Join tai JoinRace) /SendMsg
        this.writer = writer;
        String line;

        send(join);
        Global.factorK = Global.factorK_init;
        double currentThrottle = 0.0;
        double lastAngle = 0.0;
        int switchLaneTo = Global.CENTER; //Pysytään tällä kaistalla
        int totalLaps = 0;
        JSON_CarID myCar = null;
        JSON_Race myRace;
        AllCars allCars = new AllCars();
        Track track = new Track(allCars);
        boolean turboAvailable = false;
        double tobeTurboFactor = 1.0;
        double turboDurationMilliseconds = 0;
        int turboDurationTicks = 0;
        double turboFactor = 1.0;
        int turboEndsAtTick = -1;
        double newThrottle = 0.5;
        int currentGameTick = 0;
        int cntr = 0;
        int gameInitCount = 0;
        int recalculateTickCount = 0;
        int continuosBrakingTicks = 0;
        double prevSpeed = 0;

        TrackPiece lastLaneChangePiece = null;
        FileWriter fileWriter = null;

        while ((line = reader.readLine()) != null) {
            //debugInfo(line);

            //if (currentGameTick % 50 == 0) 
            //     System.out.println(line);        
            cntr++;
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            if (msgFromServer.msgType.equals("carPositions")) {
                JSON_CarPositions carPositions = gson.fromJson(line, JSON_CarPositions.class);
                TrackPiece ownCarCurrentTrackPiece = null;

                Car ownCar = null;
                currentGameTick = carPositions.gameTick;
                for (JSON_CarPosition carPosition : carPositions.data) {
                    //Missa palassa auto matkaa
                    if (carPosition.piecePosition.lap == -1) {
                        carPosition.piecePosition.lap = 0;  //Logiikka menee rikki muuten
                    }                  //      System.out.println("XXXXXXXX "+line);
                    int trackPieceIndex = carPosition.piecePosition.pieceIndex;
                    TrackPiece currentTrackPiece = track.getPiece(trackPieceIndex, carPosition.piecePosition.lap);
                    Car thisCar = allCars.findCar(carPosition.id);

                    //if ( cntr<100 )
                    //     thisCar = allCars.findCarWithLog(carPosition.id);
                    // else
                    //     thisCar = allCars.findCar(carPosition.id);
                    double travelledSoFar = thisCar.updateTravelledDistance(track, carPosition, currentTrackPiece, currentGameTick);
                    //thisCar.updateDistance(carPosition.piecePosition);
                    //TODO: Laitetaan myos muiden autojen position jne. talteen
                    if (thisCar.getIsMyCar()) {
                        ownCar = thisCar;
                        ownCarCurrentTrackPiece = currentTrackPiece;

                        double angleDiff = carPosition.angle - lastAngle; //Paljonko kulma on muuttunut
                        lastAngle = carPosition.angle;

                        int slowCarsAhead = track.slowCarsAhead();
                        if (slowCarsAhead > 0 && !currentTrackPiece.isSwitch) { //Tehdään vaihtopäätös ennen switch-palaa, jotta ei jäädä switchaamaan kaarteeseen loputtomasti
                            switchLaneTo = track.getFreeLaneBasedOnTraffic();
                            lastLaneChangePiece = ownCarCurrentTrackPiece;
                        }

                        TrackPieceLane ownTrackPieceLane = thisCar.getCurrentTrackPieceLane();

                        double currentSpeed = thisCar.getSpeed();
                        // double speedLimit2fromNow = currentTrackPiece.nextPiece.nextPiece.getMaxSpeedForLane(thisCar.tobeLaneIdx);
                        // double speedLimit1fromNow = currentTrackPiece.nextPiece.getMaxSpeedForLane(thisCar.tobeLaneIdx);
                        //double speedLimitSoon = thisCar.speedLimitAfterDistance(300);
                        // double speedLimitSoon = currentTrackPiece.getNextPiece().getMaxSpeedForLane(thisCar.getTobeLaneIdx());
                        // double speedLimitPrettySoon = currentTrackPiece.getNextPiece().getNextPiece().getNextPiece().getMaxSpeedForLane(thisCar.getTobeLaneIdx());

                        double speedLimitNow = ownTrackPieceLane.getMaxSpeed();

                        //double speedDiff2FromNow = speedLimit2fromNow - currentSpeed;
                        //double speedDiff1FromNow = speedLimit1fromNow - currentSpeed;
                        double nextSpeedLimit = thisCar.nextSpeedDecrease();
                        double distanceToNextSpeedLimit = Global.distanceToNextSpeedDecrease;
                        double speedDiffToNextSpeedLimit = thisCar.getSpeed() - nextSpeedLimit;
                        double speedInMiddleOfBraking = thisCar.getSpeed() - (speedDiffToNextSpeedLimit / 2);
                        double brakingSpeed = Global.getSpeedDecreaseForSpeed(speedInMiddleOfBraking);
                        double distanceNeededForBraking = speedInMiddleOfBraking * (speedDiffToNextSpeedLimit / brakingSpeed);

                        double speedDiffSoon = (thisCar.speedLimitAfterDistance(Math.pow(currentSpeed, 2) * Global.lookAhedForBrakingFactor) - currentSpeed);

                        //double speedDiffPrettySoon = speedLimitPrettySoon - currentSpeed;

                        /*
                         if (turboFactor > 1.0) {
                         if (speedDiffPrettySoon < speedDiffSoon) {
                         speedDiffSoon = speedDiffPrettySoon;
                         }
                         }
                         */
                        double turboFactorPow2 = Math.pow(turboFactor, 2);
                        double currentSpeedPow2 = Math.pow(currentSpeed, 2);
                        double speedLimitSoon = thisCar.speedLimitAfterDistance(currentSpeedPow2 * Global.lookAhedForBrakingFactor);
                        double speedLimitSoonWithTurbo = thisCar.speedLimitAfterDistance(currentSpeedPow2
                                * Global.lookAhedForBrakingFactor * Global.turboBreakFactor); // * turboFactorPow2

                        /*
                         if (speedLimitSoon < currentSpeed && (speedLimitSoon < speedLimitNow)) {
                         speedLimitNow = speedLimitSoon;
                         }
                         */
                        double speedDiffNow = speedLimitNow - currentSpeed;
                        double lookForBreaking;

                        if (turboFactor > 1.0) {
                            lookForBreaking = (speedLimitSoonWithTurbo - currentSpeed);
                            //debugInfo2("Turbo päällä, katsotaan eteenpäin: " + lookForBreaking + " SLAD1500=" + thisCar.speedLimitAfterDistance(1500));
                        } else {
                            lookForBreaking = (speedLimitSoon - currentSpeed);
                        }

                        if (newThrottle > 1) {
                            newThrottle = 1;
                        }
                        //lookForBreaking = 0.1;
                        if ((currentSpeed > nextSpeedLimit) && (distanceNeededForBraking >= (distanceToNextSpeedLimit - 8)) && Global.breakingSystemIsInUse()) {
                            lookForBreaking = -1;
                            if (Global.isDebug) {
                                debugInfo3("New braking rule, distanceNeeded= " + distanceNeededForBraking + " distanceToNext=" + distanceToNextSpeedLimit + " currentSpeed=" + currentSpeed + " nextSpeedLimit=" + nextSpeedLimit);
                            }
                        }
                        if (lookForBreaking < 0) { // Jarrutusmatka kasvaa nopeuden neliössä
                            newThrottle = 0; //Jarrutus mutkaan tullessa
                            continuosBrakingTicks++;

                            if ((continuosBrakingTicks == 4) || (continuosBrakingTicks == 7)) {
                                double speedDiff = prevSpeed - currentSpeed;
                                if (speedDiff > 0) {
                                    if (currentSpeed < Global.breakingSpeedMin) {
                                        Global.breakingSpeedMin = currentSpeed;
                                        Global.breakingSpeedDecreaseMin = speedDiff;
                                        Global.initSpeedDecreaseK();
                                        debugInfo3("New breakingSpeedDiffMin=" + speedDiff + " Speed=" + currentSpeed);
                                    }
                                    if (currentSpeed > Global.breakingSpeedMax) {
                                        Global.breakingSpeedMax = currentSpeed;
                                        Global.breakingSpeedDecreaseMax = speedDiff;
                                        Global.initSpeedDecreaseK();
                                        debugInfo3("New breakingSpeedDiffMax=" + speedDiff + " Speed=" + currentSpeed);
                                    }
                                }
                            }

                            prevSpeed = currentSpeed;
                        } else {
                            newThrottle += (thisCar.speedLimitAfterDistance(currentSpeed * 6) - currentSpeed) / 7.0; //Kiihdytys mutkasta ulos (11 on perushyva, 11.4 keimolaan ainakin paras)
                            continuosBrakingTicks = 0;
                        }

                        double minThrottle = 0.0;
                        if (newThrottle <= minThrottle) {
                            newThrottle = minThrottle;
                        }
                        /*
                         if (currentGameTick % 15 == 0) {
                         debugInfo(carPosition.toString() + " travelledSoFar = " + travelledSoFar + " onThisLap=" + thisCar.getDistanceOnThisLap() + " speed=" + thisCar.getSpeed() + "  Throttle=" + currentThrottle + " CurLane= " + thisCar.getLastEndLaneIndex()
                         + " SpeedLimits: " + speedLimitNow + " " + speedLimitSoon + "   speedDiffSoon=" + speedDiffSoon + "   SlowAhead:" + slowCarsAhead);
                         }
                         */
                        /*
                         int crashes = 0;
                         if (thisCar.getCrashCount() >= crashes) {
                         Global.turboBreakFactor = Global.turboBreakFactor * Global.increaseBrakingDistanceAfterCrash;
                         debugInfo2("Kasvatettiin turbojarrutuskerrointa, nyt se on: " + Global.turboBreakFactor);
                         crashes++;
                         }
                         */
                        if (currentGameTick % 25 == 0) {
                            debugInfo3(carPosition.toString() + " travelledSoFar = " + travelledSoFar + " speed=" + thisCar.getSpeed() + "  Throttle=" + currentThrottle
                                    + " SpeedLimits: " + speedLimitNow + " SpeedLimitSoon " + speedLimitSoon + "   speedDiffSoon=" + speedDiffSoon);
                        }

                    } //MyCar
                } //All Cars

                if ((Global.ownCarMaxAngle > 43) && (currentGameTick > recalculateTickCount + 300)) {
                    Global.factorK = Global.factorK * 0.96;
                    track.recalculateMaxSpeeds();
                    System.out.println("Too big Angle " + Global.ownCarMaxAngle + " ... Recalculatig max speeds with K=" + Global.factorK);
                    Global.ownCarMaxAngle = 0;
                    recalculateTickCount = currentGameTick;
                }

                if (turboEndsAtTick > -1) { //Turbo loppuu
                    if (currentGameTick > turboEndsAtTick) {
                        System.out.println("** TURBO END **");
                        turboEndsAtTick = -1;
                        turboFactor = 1.0; //Palataan takaisin norminopeuteen
                        //goRight = true;
                    }
                }

                int myLaneIdx = ownCar.getCurrentLaneIdx();

                if (currentGameTick % 50 == 0) {
                    debugInfo3("     TRACK CHECK " + ownCar + "  piece#" + ownCarCurrentTrackPiece.getPieceNumber()
                            + "  myLane=" + myLaneIdx + "  isStraight=" + ownCarCurrentTrackPiece.isStraight()
                            + " isCorner=" + ownCarCurrentTrackPiece.isCorner() + " isTight=" + ownCarCurrentTrackPiece.isTightCorner()
                            + " canUseTurbo=" + ownCarCurrentTrackPiece.canUseTurbo());
                }

                //Valitaan suunta johon yritetään kaista vaihtaa. Vaihdetaan myos, jos ollaan jo turboilemassa
                if (ownCarCurrentTrackPiece != lastLaneChangePiece) {
                    int nextTurboLaneSwitch = ownCarCurrentTrackPiece.findNextTurboPlaceAndSelectLane();
                    if ((ownCar != null) && (switchLaneTo == Global.CENTER) && (turboAvailable || (turboFactor > 1.0))) {
                        switchLaneTo = nextTurboLaneSwitch;
                        if ((myLaneIdx == 0) && switchLaneTo == Global.LEFT) //Oli jo ok
                        {
                            switchLaneTo = Global.CENTER;
                        }
                        if ((myLaneIdx == track.getLanes().size() - 1) && switchLaneTo == Global.RIGHT) //Oli jo OK
                        {
                            switchLaneTo = Global.CENTER;
                        }

                        if (switchLaneTo != Global.CENTER) {
                            System.out.println("Turbo is available, next lane=" + switchLaneTo);
                            lastLaneChangePiece = ownCarCurrentTrackPiece;
                        }
                    } else {
                        if (switchLaneTo == Global.CENTER && (ownCarCurrentTrackPiece != null) && ownCarCurrentTrackPiece.isSwitch) {
                            switchLaneTo = ownCarCurrentTrackPiece.findNextCornerAndSelectLane();
                            if ((myLaneIdx == 0) && switchLaneTo == Global.LEFT) //Oli jo ok
                            {
                                switchLaneTo = Global.CENTER;
                            }
                            if ((myLaneIdx == track.getLanes().size() - 1) && switchLaneTo == Global.RIGHT) //Oli jo OK
                            {
                                switchLaneTo = Global.CENTER;
                            }
                            if (switchLaneTo != Global.CENTER) {
                                System.out.println("NEXT CORNER based on radius, switch lane to " + switchLaneTo);
                                lastLaneChangePiece = ownCarCurrentTrackPiece;
                            }
                        }
                    }
                }

                //NYT VALITAAN LAHETETTAVA VIESTI
                if (ownCar == null) //Jos omaa autoa ei löytynyt, lähetetään vain ping
                {
                    send(new Ping());
                } else if (ownCarCurrentTrackPiece.getIsMainStraight() && (turboFactor > 1.0) && (ownCar.getCurrentLap() >= totalLaps)) { //Jos turbo päällä, Mennään kaasu pohjassa, oli mikä oli
                    send(new Throttle(1));
                } else if (Math.abs(ownCar.getAngle()) > 37) //Pikaista jarrua
                {
                    send(new Throttle(0));
                } else if (turboAvailable
                        && (newThrottle > Global.TURBO_TRIGGER_THROTTLE)
                        && (ownCarCurrentTrackPiece.canUseTurbo() != 0)
                        && ownCarCurrentTrackPiece.isStraight()
                        && (!ownCarCurrentTrackPiece.isTightCorner())
                        && (!ownCarCurrentTrackPiece.getNextPiece().isTightCorner())
                        && ownCarCurrentTrackPiece.getNextPiece().isStraight()
                        //&& (ownCar.speedLimitAfterDistance(400) > 6) //Ile lisasi, jotta ei lahtisi mutkassa
                        //&& (ownCar.speedLimitAfterDistance(0) > 8)
                        //&& (ownCar.speedLimitAfterDistance(200) > 8)
                        ) {
                    turboAvailable = false;
                    turboFactor = tobeTurboFactor;
                    turboEndsAtTick = currentGameTick + turboDurationTicks;
                    send(new Turbo());
                    System.out.println("** TURBO ON ** " + ownCarCurrentTrackPiece.isStraight() + ownCarCurrentTrackPiece.getNextPiece().isStraight() + ownCarCurrentTrackPiece.getNextPiece().getNextPiece().isStraight());
                    //newThrottle = 1000;
                } else if ((switchLaneTo != Global.CENTER) && (ownCarCurrentTrackPiece != null) && ownCarCurrentTrackPiece.isSwitch) {
                    /*if (switchLaneTo == Global.LEFT) {
                     debugInfo2("Yritettiin vaihtaa kaistaa vasemmalle");
                     } else {
                     debugInfo2("Yritettiin vaihtaa kaistaa oikealle");
                     }*/
                    System.out.println("Switching lane to " + switchLaneTo + " before corner " + ownCarCurrentTrackPiece.getNextCornerNumber());
                    send(new GoLeftOrRight(switchLaneTo == Global.LEFT));
                    switchLaneTo = Global.CENTER;
                } else if (newThrottle != currentThrottle) {

                    currentThrottle = newThrottle;
                    //if (currentThrottle > 1) {
                    //    currentThrottle = 1;
                    //}
                    double T = 1 / turboFactor * currentThrottle;
                    if (T > 1.0) {
                        T = 1.0;
                    }
                    if (Global.isDebug && fileWriter != null) {
                        //fileWriter.write("Throttle;Speed;SpeedLimit;CarAngle;Radius;Lane;Turbo\n");
                        fileWriter.write(T + ";" + ownCar.getSpeed() + ";" + ownCar.getCurrentTrackPieceLane().getMaxSpeed() + ";" + ownCar.getAngle() + ";" + ownCar.getCurrentTrackPieceLane().getOwnerTrackPiece().getRadius() + ";" + ownCar.getCurrentLaneIdx() + ";" + turboFactor + "\n");
                        if (currentGameTick % 1000 == 0) {
                            fileWriter.flush();
                        }
                    }
                    send(new Throttle(T));

                } else {

                    //send(new Throttle(currentThrottle));
                    double T = 1 / turboFactor * currentThrottle;
                    if (T > 1.0) {
                        T = 1.0;
                    }
                    if (Global.isDebug && fileWriter != null) {
                        fileWriter.write(T + ";" + ownCar.getSpeed() + ";" + ownCar.getCurrentTrackPieceLane().getMaxSpeed() + ";" + ownCar.getAngle() + ";" + ownCar.getCurrentTrackPieceLane().getOwnerTrackPiece().getRadius() + ";" + ownCar.getCurrentLaneIdx() + ";" + turboFactor + "\n");

                        if (currentGameTick % 1000 == 0) {
                            fileWriter.flush();
                        }
                    }
                    send(new Throttle(T));
                }

            } else if (msgFromServer.msgType.equals("join")) {
                debugInfo("Joined");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
                //goLeft = true;
                try {
                    JSON_TurboAvailable myTurbo = gson.fromJson(gson.toJson(msgFromServer.data), JSON_TurboAvailable.class);
                    turboAvailable = true;
                    turboDurationMilliseconds = myTurbo.turboDurationMilliseconds;
                    turboDurationTicks = myTurbo.turboDurationTicks;
                    tobeTurboFactor = myTurbo.turboFactor;
                    debugInfo2("Turbo available!!! factor=" + tobeTurboFactor);
                } catch (Exception e) {
                    System.out.println("Turbo problem: " + e + " " + msgFromServer.data);
                }

                send(new Ping());
            } else if (msgFromServer.msgType.equals("yourCar")) {
                debugInfo("Your Car " + msgFromServer.data);
                myCar = gson.fromJson(gson.toJson(msgFromServer.data), JSON_CarID.class);
                debugInfo(myCar.toString());
                send(new Ping());
            } else if (msgFromServer.msgType.equals("crash")) {
                JSON_CarID crashedCarJSON = gson.fromJson(gson.toJson(msgFromServer.data), JSON_CarID.class);
                Car crashedCar = allCars.findCar(crashedCarJSON);
                boolean bumbedOut = track.isThereOtherCarBehindCar(crashedCar);
                crashedCar.setCrashCount(crashedCar.getCrashCount() + 1);
                System.out.println("Crash #" + crashedCar.getCrashCount() + " for " + crashedCar + "  otherBack=" + bumbedOut);

                if (crashedCar.getIsMyCar()) {
                    System.out.println("Max angle before crash = " + Global.ownCarMaxAngle);

                    if (!bumbedOut) {
                        Global.factorK = Global.factorK * 0.94;
                        Global.lookAhedForBrakingFactor = Global.lookAhedForBrakingFactor * Global.increaseBrakingDistanceAfterCrash;
                        track.recalculateMaxSpeeds();
                        System.out.println("New FactorK=" + Global.factorK + "  lookAhedForBrakingFactor=" + Global.lookAhedForBrakingFactor);

                    }
                    Global.ownCarMaxAngle = 0;
                }

                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameInit")) {
                debugInfo("Race init");
                gameInitCount++;
                //System.out.println(line);
                try {
                    myRace = (gson.fromJson(gson.toJson(msgFromServer.data), JSON_GameInit.class).race);
                } catch (JsonSyntaxException e) {
                    System.out.println("Error: " + e);
                    System.out.println(msgFromServer.data.toString());
                    myRace = (gson.fromJson(gson.toJson(msgFromServer.data), JSON_GameInit.class).race);
                }

                totalLaps = myRace.raceSession.laps;
                if (totalLaps == 0) {
                    totalLaps = 10; //Aika-ajoja varten
                }
                //Jos tullaan kisaan, poistetan alta vanhat
                allCars.removeAllCars();
                track.removeAllPieces();
                track.InitRace();

                if (gameInitCount == 2) { //Otetaan vähän vauhtia lisää kisaan
                    if (Global.INCREASE_K_FOR_RACE != 1.0) {
                        Global.factorK = Global.factorK * Global.INCREASE_K_FOR_RACE;
                        track.recalculateMaxSpeeds();
                        System.out.println("RACE-K = " + Global.factorK);
                    }
                }

                debugInfo("Lanes = " + myRace.track.lanes);
                for (JSON_Lane oneLane : myRace.track.lanes) {
                    track.getLanes().add(new Lane(oneLane));
                }

                debugInfo2("myCar=" + myCar);

                for (JSON_Car oneCar : myRace.cars) {
                    Car newCar = allCars.addCar(oneCar.id, oneCar.dimensions);
                    newCar.setIsMyCar(newCar.isSameAs(myCar));
                    if (newCar.getIsMyCar()) {
                        track.setMyCar(newCar);
                    }
                }
                track.fillOpponents();

                debugInfo("Track=" + myRace.track.name);

                if (Global.isDebug) {
                    File fileDebug = new File("./car-run-" + myRace.track.name + ".csv");
                    fileWriter = new FileWriter(fileDebug);
                    fileWriter.write("Throttle;Speed;SpeedLimit;CarAngle;Radius;Lane;Turbo\n");
                }

                track.setTrackPieceCount(myRace.track.pieces.size());

                System.out.println("Track=" + myRace.track.name + " pieces=" + track.getTrackPieceCount());
                TrackPiece lastPiece = null;
                TrackPiece newTrackPiece;
                TrackPiece firstTrackPiece = null;
                boolean firstRound = true;
                boolean stillIsMainStaight = false;
                for (int lap = 0; lap < totalLaps; lap++) {
                    int cornerNumber = 0; //Laskee mutkien määriä, mutta alkuperäisellä numeroinnilla, eli toistuu kierros kierrokselta
                    int pieceNumber = 0; //Laskee palojen määriä, mutta alkuperäisellä numeroinnilla, eli toistuu kierros kierrokselta
                    boolean isFirstPiece = true;
                    for (JSON_TrackPiece trackPiece : myRace.track.pieces) {
                        newTrackPiece = track.addPiece(trackPiece, lap, lastPiece);
                        pieceNumber++;
                        newTrackPiece.setPieceNumber(cornerNumber);
                        if (newTrackPiece.isCorner()) {
                            cornerNumber++;
                            newTrackPiece.setCornerNumber(cornerNumber);
                            if (firstRound) {
                                System.out.println("Corner " + cornerNumber + " radius=" + trackPiece.radius + " angle=" + trackPiece.angle + " isTight=" + newTrackPiece.isTightCorner());
                            }
                        }

                        if (isFirstPiece) {
                            isFirstPiece = false;
                            newTrackPiece.setIsMainStraight(true);
                            stillIsMainStaight = true;
                        }
                        if (!newTrackPiece.isStraight()) {
                            stillIsMainStaight = false;

                        } else {
                            newTrackPiece.setIsMainStraight(stillIsMainStaight);
                        }
                        if (firstTrackPiece != null) {
                            firstTrackPiece = newTrackPiece;
                        }
                        if (lastPiece != null) {
                            newTrackPiece.setPrevPiece(lastPiece);
                            lastPiece.setNextPiece(newTrackPiece);
                        }
                        lastPiece = newTrackPiece;
                    }
                    firstRound = false;
                }

                track.completeMainStarightFlagging(); //Paasuora on merkattu vain lahdosta eteenpain, tama taydentaa loput
                track.calculateAngleAhead();
                track.printPieces();
                debugInfo2(track.toString());
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                debugInfo("Race end");
                send(new Ping());
            } else if (msgFromServer.msgType.equals("gameStart")) {
                debugInfo("Race start");
                send(new Ping());
            } else {
                send(new Ping());
            }
        }
        //Tulostetaan pelin paattymisen tilastot
        allCars.reportStatistics();
        if (Global.isDebug && fileWriter != null) {
            fileWriter.close();
        }
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        //debugInfo(msg.toJson());
        writer.flush();
    }

    private void debugInfo(String info) {
        //System.out.println(info);
    }

    private void debugInfo2(String info) {
        System.out.println(info);
    }

    private void debugInfo3(String info) {
        System.out.println(info);
    }

}

abstract class SendMsg {

    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class MsgWrapper {

    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {

    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class BotID {

    public String name;
    public String key;

    public BotID(String name, String key) {
        this.name = name;
        this.key = key;
    }
}

class JoinRace extends SendMsg {

    public BotID botId;
    public String trackName;
    public int carCount;

    JoinRace(final String name, final String key, String trackName, int carCount) {
        botId = new BotID(name, key);
        this.trackName = trackName;
        this.carCount = carCount;
    }

    @Override
    protected String msgType() {
        return "joinRace";
    }
}

class Ping extends SendMsg {

    @Override
    protected String msgType() {
        return "ping";
    }
}

class Throttle extends SendMsg {

    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class Turbo extends SendMsg {

    @Override
    protected Object msgData() {
        return "Ice man mode";
    }

    @Override
    protected String msgType() {
        return "turbo";
    }
}

class GoLeftOrRight extends SendMsg {

    private final boolean goLeft;

    public GoLeftOrRight(boolean goLeft) {
        this.goLeft = goLeft;
    }

    @Override
    protected Object msgData() {
        if (this.goLeft) {
            return "Left";
        } else {
            return "Right";
        }
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}
